package sbu.cs.parser.html;

public class HTMLParser {

    public static Node parse(String sample) {
        sample = sample.replaceAll("\n", "").trim();
        Node node = new Node(sample);
        sample = removeTag(sample);

        if(!sample.contains("<"))
            return node;

        while(sample.length() != 0) {
            node.addChildren(openingTag(sample));
            sample = deleteTag(sample);
        }

        return node;
    }

    private static String removeTag(String html) {
        String openingTag = html.substring(html.indexOf('<'), html.indexOf('>') + 1);

        int idx = Math.min(html.indexOf('>'), html.indexOf(' '));
        int index;

        if (idx == -1)
            index = html.indexOf('>');
        else
            index = idx;

        String tagName = html.substring(html.indexOf('<') + 1, index);
        String closingTag = "</" + tagName + ">";

        return html.replaceFirst(openingTag, "").replaceFirst(closingTag, "");
    }

    private static Node openingTag(String html) {
        String tmp = html.substring(html.indexOf('<'), html.indexOf('>') + 1);

        if((html.indexOf('<') + 1) == (html.indexOf('/')))
            return new Node(tmp.trim());
        else {
            String openingTag = tmp;
            String tagName = html.substring(html.indexOf('<') + 1, Math.min(html.indexOf('>'), html.indexOf(' ')));
            String closingTag = "</" + tagName + ">";
            String tag = html.substring(html.indexOf(openingTag) + openingTag.length(), html.indexOf(closingTag)).trim();

            return new Node(openingTag + tag + closingTag);
        }
    }

    private static String deleteTag(String html) {
        String temp = html.substring(html.indexOf('<'), html.indexOf('>') + 1);

        if((html.indexOf('<') + 1) == (html.indexOf('/')))
            return html.replaceFirst(temp, "").trim();
        else {
            String openingTag = temp;
            String tagName = html.substring(html.indexOf('<') + 1,
                    Math.min(html.indexOf('>'), html.indexOf(' ')));
            String closingTag = "</" + tagName + ">";
            String tag = html.substring(html.indexOf(openingTag) + openingTag.length(),
                                            html.indexOf(closingTag));

            return html.replaceFirst(openingTag + tag + closingTag, "").trim();
        }
    }
}
