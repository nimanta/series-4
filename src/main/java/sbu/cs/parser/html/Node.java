package sbu.cs.parser.html;

import java.util.*;

public class Node implements NodeInterface {

    private List<Node> children = new ArrayList<>();
    private String sample;
    private Map<String, String> attributes = new HashMap<>();

    public Node(String sample) {
        this.sample = sample;

        try {
            attribute();
        }
        catch(Exception e) {}
    }

    private void attribute() throws Exception {
        if(!sample.contains("="))
            return;

        String []attrs = sample.substring(sample.indexOf(" ") + 1, sample.indexOf(">") + 1)
                .replace('>', ' ').split("\" ");

        for (String attr : attrs) {
            attr = attr.replaceAll("\"", "");
            String []splitted = attr.split("=");
            attributes.put(splitted[0], splitted[1]);
        }
    }

    @Override
    public String getAttributeValue(String key) {
        return attributes.get(key);
    }

    public void addChildren(Node child) {
        children.add(child);
    }

    @Override
    public List<Node> getChildren() {
        for(int i = 0; i < children.size(); i++) {
            Node attrs = HTMLParser.parse(children.get(i).sample);
            children.set(i, attrs);
        }
        return children;
    }

    @Override
    public String getStringInside() {
        if((sample.indexOf('<') + 1) == (sample.indexOf('/')))
            return null;

        String openingTag = sample.substring(sample.indexOf('<'), sample.indexOf('>') + 1);
        String tagName = sample.substring(sample.indexOf('<') + 1,
                             Math.min(sample.indexOf('>'), sample.indexOf(' ')));
        String closingTag = "</" + tagName + ">";

        return sample.substring(sample.indexOf(openingTag) + openingTag.length(),
                                                sample.indexOf(closingTag)).trim();
    }

}
