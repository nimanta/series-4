package sbu.cs.parser.json;

public class Json implements JsonInterface {

    public JsonObject jsonObject;

    public Json(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    @Override
    public String getStringValue(String key) {

        for(int i = 0; i < jsonObject.items.size(); i++) {
            if(jsonObject.items.get(i) instanceof JsonArray) {
                if(((JsonArray) jsonObject.items.get(i)).key.equals(key)) {
                    StringBuilder arr = new StringBuilder();
                    for(Object obj : (((JsonArray) jsonObject.items.get(i)).value)){
                        arr.append(obj.toString());
                        arr.append(", ");
                    }

                    arr.delete(arr.length() - 2, arr.length());
                    return arr.toString();
                }
            }

            else if(jsonObject.items.get(i) instanceof JsonBoolean) {
                if (((JsonBoolean) jsonObject.items.get(i)).key.equals(key))
                    return String.valueOf(((JsonBoolean) jsonObject.items.get(i)).value);
            }

            else if(jsonObject.items.get(i) instanceof JsonDouble) {
                if(((JsonDouble) jsonObject.items.get(i)).key.equals(key))
                    return String.valueOf(((JsonDouble) jsonObject.items.get(i)).value);
            }

            else if(jsonObject.items.get(i) instanceof JsonInt) {
                if(((JsonInt) jsonObject.items.get(i)).key.equals(key))
                    return String.valueOf(((JsonInt) jsonObject.items.get(i)).value);
            }

            else if(jsonObject.items.get(i) instanceof JsonString) {
                if(((JsonString) jsonObject.items.get(i)).key.equals(key))
                    return ((JsonString) jsonObject.items.get(i)).value;
            }
        }
        return "";
    }
}