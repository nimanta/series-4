package sbu.cs.parser.json;

import java.util.*;

public class JsonObject {


	public JsonObject() {

	}

	ArrayList<Object> items = new ArrayList();

	public void addJsonArray(String key, ArrayList value) {
		items.add(new JsonArray(key, value));
	}

	public void addJsonBoolean(String key, boolean value) {
		items.add(new JsonBoolean(key, value));
	}

	public void addJsonDouble(String key, double value) {
		items.add(new JsonDouble(key, value));
	}

	public void addJsonInt(String key, int value) {
		items.add(new JsonInt(key, value));
	}

	public void addJsonString(String key, String value) {
		items.add(new JsonString(key, value));
	}

}