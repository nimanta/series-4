package sbu.cs.parser.json;

public class JsonBoolean {

	String key;
	boolean value;

	public JsonBoolean(String key, boolean value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return Boolean.toString(value);
	}

}