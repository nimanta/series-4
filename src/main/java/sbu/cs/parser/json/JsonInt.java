package sbu.cs.parser.json;

public class JsonInt {

	String key;
	int value;

	public JsonInt(String key, int value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return Integer.toString(value);
	}
}