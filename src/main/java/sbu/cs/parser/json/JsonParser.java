package sbu.cs.parser.json;

import java.util.*;

public class JsonParser {

    static Json json = new Json(new JsonObject());

    public static Json parse(String keyValue) {

        keyValue = keyValue.replaceAll("[\\s{}\"]*", "");
        String copyOfJString = keyValue;

        while (copyOfJString.indexOf('[') >= 0) {
            String str = copyOfJString.substring(copyOfJString.indexOf("["),
                                                  copyOfJString.indexOf("]") + 1);
            keyValue = keyValue.replace(str, str.replaceAll(",", "#"));
            copyOfJString = copyOfJString.replace(str, "");
        }

        String[] keyValues = keyValue.split(",");

        whatKind(keyValues);

        return json;
    }

    private static void whatKind(String[] keyValues) {

        for (String keyValue : keyValues) {
            String[] splitted = keyValue.split(":");
            String key = splitted[0];
            String value = splitted[1];

            if(value.indexOf("]") >= 0) {
                ArrayList<Object> array = new ArrayList<>();
                String []values = value.split("#");
                for (String val : values)
                    array.add(val);

                json.jsonObject.addJsonArray(key, array);
            }

            else if(value.matches("(true|false)"))
                json.jsonObject.addJsonBoolean(key, Boolean.parseBoolean(value));

            else if(value.matches("\\d*\\.\\d*"))
                json.jsonObject.addJsonDouble(key, Double.parseDouble(value));

            else if(value.matches("\\d*"))
                json.jsonObject.addJsonInt(key, Integer.parseInt(value));

            else
                json.jsonObject.addJsonString(key, value);
        }
    }







}