package sbu.cs.parser.json;

public class JsonDouble {

	String key;
	Double value;

	public JsonDouble(String key, Double value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return Double.toString(value);
	}
}