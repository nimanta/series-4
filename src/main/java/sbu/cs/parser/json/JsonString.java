package sbu.cs.parser.json;

public class JsonString {

	String key;
	String value;

	public JsonString(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}
}